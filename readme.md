# Maison Sport Code Standards & Tooling config

## Installing tools globally

### Install Tools

First, clone the standards repository **in your home directory**.

```bash
cd ~
git clone git@gitlab.com:maisonsport/standards.git
```

Install some dependencies:

```bash
composer global require hirak/prestissimo
composer global require squizlabs/php_codesniffer
composer global require phpmd/phpmd
composer global require slevomat/coding-standard
```

then ensure the composer bin directory in in your PATH in your ~/.zshrc like by running the command:
```bash
echo 'export PATH="$HOME/.composer/vendor/bin:$PATH"' >> ~/.zshrc
```

### Set up PHPStorm

Navigate to: `File -> Import Settings...`

Select `PHPStorm/settings.zip` from this repository.

Configure the paths to your phpcs, phpcbf and phpmd binaries in PHPstorm here:
`Preferences | Languages & Frameworks | PHP | Quality Tools`
each path is simply the relevant binary name from above as they are in your PATH.

## Updating

To update the code style and inspection definitions git pull this repo

## JS standards
Our Core JS standards 'config is hosted on `npm` - For more detail, see the `readme.md` in the `eslint/` directory.
