# eslint-config-maisonsport
## install
```bash
npm i --save-dev eslint-config-maisonsport
npx install-peerdeps --dev eslint-config-maisonsport
```
## .eslintrc.js
```javascript
{
    extends: 'eslint-config-maisonsport',
    ...
}
```
