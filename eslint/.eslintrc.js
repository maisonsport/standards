module.exports = {
  extends: 'airbnb',
  parserOptions: {
    ecmaVersion: 11,
  },
  plugins: ['jest', 'react-hooks'],
  env: {
    'jest/globals': true,
  },
  rules: {
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'no-use-before-define': ['error', { variables: false }],
    'global-require': [0],
    'react/prop-types': [0],
    'react-hooks/exhaustive-deps': 'warn',
  },
};
